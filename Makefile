abc.out : abc.o a.o b.o libctest.a libctest_sh.so 
	g++ -o abc.out abc.o  libctest.a ./libctest_sh.so
abc.o : abc.cpp mylib.h
	g++ -c abc.cpp
a.o : a.cpp
	g++ -Wall -c a.cpp
b.o : b.cpp
	g++ -Wall -fPIC -c b.cpp
libctest.a : a.o
	ar rcs libctest.a a.o
libctest_sh.so : b.o
	g++ b.o -shared -o libctest_sh.so
clean:
	rm *.o libctest.a  libctest_sh.so abc.out

