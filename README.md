This is training project to test CMake in order to build small program calling 
two functions from static and shared libraries:
1) abc.cpp - main function;
2) a.cpp - function added to static library;
3) b.cpp - function added to shared library.